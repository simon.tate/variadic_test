#define _GNU_SOURCE
#include <stdio.h>
#include <stdarg.h>
#include <syslog.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>

#define CRIT(format, ...) LOG(LOG_CRIT, format, ##__VA_ARGS__)

#define ERR(format, ...) LOG(LOG_ERR, format, ##__VA_ARGS__)

#define WARN(format, ...) LOG(LOG_WARNING, format, ##__VA_ARGS__)

#define INFO(format, ...) LOG(LOG_INFO, format, ##__VA_ARGS__)

#define DBG(format, ...) LOG(LOG_DEBUG, format, ##__VA_ARGS__)

#define LOG(level, format, ...) my_log(level, #level, __FILE__, __func__, __LINE__, format, ##__VA_ARGS__)

#define MY_CHECKED_FREE(ptr)    \
    if (ptr)                        \
        free(ptr);                  \
    ptr = NULL;

void my_log(int level, const char *level_name, const char *file, const char *function, int line, char *format, ...)
{
    va_list args;
    va_start(args, format);

    char *buf = NULL;

    int result = vasprintf(&buf, format, args);
    if (result >= 0) {
        syslog(level, "%s(%d) %s : %s\n", file, line, function, buf);

        if (true) {
            fprintf(stderr, "%ld %11s %-32s(%3d) %-24s : %s\n", time(NULL), level_name, file, line,
                    function, buf);
        }
    }

    MY_CHECKED_FREE(buf);
    va_end(args);
}

int main(int argc, char **argv)
{
    CRIT("%s number of specifiers does not meet number of passed arguments %s", "dummy");
    ERR("%s number of specifiers does not meet number of passed arguments %s", "dummy");
    WARN("%s number of specifiers does not meet number of passed arguments %s", "dummy");
    INFO("%s number of specifiers does not meet number of passed arguments %s", "dummy");
    DBG("%s number of specifiers does not meet number of passed arguments %s", "dummy");

    /* intentional overflow */

    unsigned a = 0xffffffff, b = 0x1, c = 0;

    c = a + b;

    (void)c;

    return 0;
}
